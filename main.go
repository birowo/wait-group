package main

import (
	"sync"
	"sync/atomic"
	"time"
)

type WaitGroup struct {
	sync.Mutex
	delta int32
}

func (wg *WaitGroup) Add(delta int) {
	wg.delta += int32(delta)
	if wg.delta < 1 {
		panic("err: negative wait group delta")
	}
	wg.TryLock()
}
func (wg *WaitGroup) Done() {
	if atomic.AddInt32(&wg.delta, -1) == 0 {
		wg.Unlock()
	}
}
func (wg *WaitGroup) Wait() {
	wg.Lock()
	wg.Unlock()
}
func main() {
	n := 1000
	wg := new(WaitGroup)
	wg.Add(n)
	for i := 0; i < n; i++ {
		go func(i int) {
			time.Sleep(time.Second)
			println(i)
			wg.Done() //jika ini dikomen akan deadlock
		}(i)
	}
	wg.Wait() //jika ini dikomen akan langsung keluar, goroutine tidak sempat print
}
